<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/1/14
 * Time: 11:56 AM
 */

namespace Filter\TinyCrypt;


use Zend\Filter\FilterInterface;

class Decrypt extends AbstractTinyCrypt implements FilterInterface {

    /**
     * @var string
     */
    protected $key = 'clicklogicaltinyul';

    /**
     * @var int
     */
    protected $length = 5;

    /**
     * Constructor
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
        parent::__construct($this->getKey(), $this->getLength());
    }

    /**
     * Decrypts a tiny Url
     * @param mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        return array_pop($this->decrypt($value));
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param int $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }
} 