<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 2:56 PM
 * Project: filter
 */
namespace Filter;
use Zend\Filter\AbstractFilter;
use Zend\Validator\Uri;

/**
 * Class StringToImage
 * @package Filter
 */
class StringToImage extends AbstractFilter {

    /**
     * Orientation Flag 6
     */
    const ROTATE_180 = 180;

    /**
     * Orientation Flag 3
     */
    const ROTATE_90_CCW = 270;

    /**
     * Orientation Flag 8
     */
    const ROTATE_90_CW = 90;

    /**
     * Directory to store the images in
     * @var string
     */
    protected $target;

    /**
     * Flag for orientation correction
     * @var bool
     */
    protected $correctOrientation;

    /**
     * @var boolean
     */
    protected $randomize;

    /**
     * The maximum allowed size
     * @var array
     */
    protected $maximumSize;

    /**
     * Stores the images processed on this request.
     * We do this because if the filter is consumed by an input filter,
     * the input filter may filter the data more than once, creating multiple images
     * of the same content
     * @var array
     */
    protected static $processedImages = array();

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @return mixed
     */
    public function getCorrectOrientation()
    {
        return $this->correctOrientation;
    }

    /**
     * @param mixed $correctOrientation
     */
    public function setCorrectOrientation($correctOrientation)
    {
        $this->correctOrientation = $correctOrientation;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget($target)
    {
        $target = str_ireplace(array('/', '\\'), DIRECTORY_SEPARATOR, $target);

        if( substr($target, -1) != DIRECTORY_SEPARATOR )
        {
            $target .= DIRECTORY_SEPARATOR;
        }

        $this->target = $target;
    }

    /**
     * @return boolean
     */
    public function isRandomize()
    {
        return $this->randomize;
    }

    /**
     * @param boolean $randomize
     */
    public function setRandomize($randomize)
    {
        $this->randomize = $randomize;
    }

    /**
     * @return array
     */
    public function getMaximumSize()
    {
        return $this->maximumSize;
    }

    /**
     * @param array $maximumSize
     */
    public function setMaximumSize($maximumSize)
    {
        $this->maximumSize = $maximumSize;
    }

    /**
     * Receives posted images as raw data and makes them image files
     * @param mixed $imageData
     * @throws \Exception
     * @return mixed|string
     */
    public function filter($imageData)
    {
        if( $imageData == null || $imageData == '' )
        {
            return $imageData;
        }

        if( is_string($imageData) )
        {
            return $this->processImage($imageData);
        }
        elseif( is_array($imageData) )
        {
            $filenames = array();

            foreach($imageData as $realImageData)
            {
                $filenames[] = $this->processImage($realImageData);
            }

            return $filenames;
        }

        throw new \Exception('Image Data was not a string or an array');
    }

    /**
     * Processes the string to an image file
     * @param $imageData
     * @throws \Exception
     * @return string
     */
    protected function processImage($imageData)
    {
        $uri = new Uri(array('allowRelative'=>true, 'allowAbsolute'=>false));

        if( $uri->isValid($imageData) ) // if it is a uri, we are probably passing in a link to an image
        {
            return $imageData;
        }

        if( $key = array_search($imageData, self::$processedImages) )
        {
            return $key;
        }

        if( $this->getTarget() == null )
        {
            throw new \Exception('Source Directory not set');
        }

        $extension = $this->getExtensionForStream($imageData);

        // data:image/EXT;base64 = 18 + COUNT(EXT)
        $imageString = substr($imageData, 19 + strlen($extension));
        $imageString = base64_decode($imageString);

        $rotation = false;
        $microtime = microtime(false) . uniqid();

        // Do the orientation detection prior to making the image
        if( $this->getCorrectOrientation() ) {

            $filename = str_replace(array('.', ' '), '', $microtime) . '.'.$extension;
            $temporaryTarget = $this->getTarget() . $filename;

            $tempFileHandler = fopen($temporaryTarget, 'w+');
            fwrite($tempFileHandler, $imageString);
            fclose($tempFileHandler);

            // Check file type
            $metaData = null;
            if(in_array(mime_content_type($temporaryTarget), ['image/jpeg', 'image/tiff'])){
                try{
                    $metaData = exif_read_data($temporaryTarget);
                } finally{
                    $metaData = null;
                }
            }
            unlink($temporaryTarget);

            if (is_array($metaData) && isset($metaData['Orientation']) && $metaData['Orientation'] != 1) {
                $rotation = $metaData['Orientation'];
            }
        }

        $result = \imagecreatefromstring($imageString);
        if ($result !== false) {
            $filename = str_replace(array('.', ' '), '', $microtime) . '.'.$extension;
            $target = $this->getTarget() . $filename;

            \imagealphablending($result, true);

            if ($rotation)
            {
                switch ($rotation)
                {
                    case 3:
                    {
                        $result = \imagerotate($result, self::ROTATE_180, 0);
                        break;
                    }
                    case 8:
                    {
                        $result = \imagerotate($result, self::ROTATE_90_CW, 0);
                        break;
                    }
                    case 6:
                    {
                        $result = \imagerotate($result, self::ROTATE_90_CCW, 0);
                        break;
                    }
                }
            }

            $imageResult = $this->renderImageForExtension($result, $target, $extension);
            \imagedestroy($result);

            if( $imageResult == false )
            {
                throw new \Exception('An error occurred while trying to write the image');
            }

            chmod($target, 0777);

            if( is_array($this->getMaximumSize()) )
            {
                $config = $this->getMaximumSize();

                if( ! isset($config['width']) || ! isset($config['height']))
                {
                    throw new \Exception('Width or Height not set');
                }

                $thumbnailer = new Image();
                $image = $thumbnailer->create($target);
                $image->resize($config['width'], $config['height']);
                $image->save($target);
            }

            self::$processedImages[$target] = $imageData;

            return $target;
        }
        else
        {
            throw new \Exception('An error occurred while trying to create an image from a string');
        }
    }

    /**
     * process the mime-type in the front of the stream to determine file extension
     * @param $stream
     * @return mixed|string
     */
    protected function getExtensionForStream($stream) {
        // mime => extension
        $extensions = array(
            'jpeg' => 'jpg',
            'jpg' => 'jpg',
            'gif' => 'gif',
            'png' => 'png',
        );

        $extension = '';
        // start after data:image/ to grab mime file type, and look 4 characters forward
        // a small chunk makes it less resource-heavy
        $mimeChunk = substr($stream, 11, 4);
        foreach($extensions as $_mime => $_extension) {
            if(strpos($mimeChunk, $_mime) >= 0) {
                $extension = $_extension;
                break;
            }
        }
        return $extension;
    }

    /**
     * @param $image
     * @param $target
     * @param $extension
     * @return bool
     */
    protected function renderImageForExtension($image, $target, $extension) {
        switch($extension) {
            case ('jpg'):
                return \imagejpeg($image, $target, 75);
                break;
            case('png'):
                return \imagepng($image, $target, 0);
                break;
            case('gif'):
                return \imagegif($image, $target);
        }
        return false;
    }
}